
import java.util.HashMap;
import java.util.*;

/**
 * classe BaseDeDonnees
 * Contient une hashmap de chasseurs et une hashmap de pilotes
 * CLés identifiants
 * @author Thierry
 */
public class BaseDeDonnee
{
    HashMap<Integer,Chasseur> chasseurs;
    HashMap<Integer, Pilote> pilotes;
    Chasseur chasseur;
    Pilote pilote;
    
    /**
     * Constructeur d'objets de classe BaseDeDonnee
     */
    public BaseDeDonnee()
    {
        this.chasseurs = new HashMap<Integer,Chasseur>();
        this.pilotes = new HashMap<Integer, Pilote>();
    }

    
    /**
     * Affichage de tous les chasseurs de la base
     *  @author Thierry
     */
    public String ListerChasseurs()
    {
        String strChasseurs = "AFFICHAGE DE TOUS LES CHASSEURS:\n"+
                       "Nombre dans la base militaire: " + chasseurs.size() + "\n"+
                       "-------------------------------------------------------------\n";
        for (Chasseur chasseur : chasseurs.values()) 
        {
        strChasseurs += "Identifiant chasseur: n°" + chasseur.idChasseur + "\n" +
                        "Type: " + chasseur.type + "\n" +
                        "Etat: " + chasseur.etat + "\n" +
                        "-------------------------------------------------------------\n";
        }
        return strChasseurs;
    }
    
    /**
     * Affichage de tous les pilotes de la base
     *  @author Thierry
     */
    public String ListerPilotes()
    {
        String strPilotes = "AFFICHAGE DE TOUS LES PILOTES:\n"+
                            "Nombre de pilotes formés: " +pilotes.size()+ "\n"+
                            "---------------------------------------------------------------------\n";
        for (Pilote pilote : pilotes.values()) 
        {
        strPilotes += "Identifiant pilote: n°" + pilote.idPilote + "\n" +
                      "Nom: " + pilote.nomRebelle + "\n" +
                      "Prénom: " + pilote.prenomRebelle + "\n" +
                      "Age: " + pilote.ageRebelle + "\n"+
                       afficherChasseurPilote(pilote) +
                      "---------------------------------------------------------------------\n";
        } 
        return strPilotes;
    }
    
    /**
     * Affichage d'un seul chasseur
     *  @author Thierry
     */
    
    public String afficherChasseur(int idChasseur)
        
    {
        chasseur = chasseurs.get(idChasseur);
        String strChasseur = "---------------------------------------------------------------------\n" +
                             "AFFICHAGE DU CHASSEUR N°"+ chasseur.idChasseur + "\n" +
                             "Type: " + chasseur.type + "\n" +
                             "Etat: " + chasseur.etat + "\n" +
                             "---------------------------------------------------------------------\n";
        return strChasseur;
    }
    
    /**
     * Affichage d'un seul pilote
     *  @author Thierry
     */
    
    public String afficherPilote(int idPilote)
        
    {
        pilote = pilotes.get(idPilote);
        String strPilote = "---------------------------------------------------------------------\n" +
                           "AFFICHAGE DU PILOTE N°"+ pilote.idPilote + "\n" +
                           "Nom: " + pilote.nomRebelle + "\n" +
                           "Prénom: " + pilote.prenomRebelle + "\n" +
                           "Age: " + pilote.ageRebelle + "\n"+
                           afficherChasseurPilote(pilote) +
                           "---------------------------------------------------------------------\n";
        return strPilote;
    }
    
    
    /**
     * Affichage de tous les chasseurs opérationnels
     *  @author Thierry
     */
    public String ListerChasseursOperationnels(String[] tabEtats)
    {
        String strPilotesOpe =  "AFFICHAGE DE TOUS LES CHASSEURS OPERATIONNELS:\n" +
                                "---------------------------------------------------------------------\n";
        for (Chasseur chasseur : chasseurs.values()) 
        {
            if (chasseur.etat.equals(tabEtats[0]))
            {
                strPilotesOpe += "Chasseur n°:"+ chasseur.idChasseur + "\n" +
                                "Type: " + chasseur.type + "\n" +
                                "Etat: " + chasseur.etat + "\n" +
                                "---------------------------------------------------------------------\n";
                }
            }
        return strPilotesOpe;
    }
    
    /**
     * Méthode qui permet de vérifier si un pilote est affecté à un chasseur
     */
    public String afficherChasseurPilote(Pilote pilote)
    {
        String strChasseurPilote;
        if (pilote.estAffecte == true)
        {
            strChasseurPilote = "Affecté au chasseur n°" + pilote.chasseur.idChasseur + " de type " + pilote.chasseur.type + " dont l'état est "+ pilote.chasseur.etat + "\n";
        } else
        {
            strChasseurPilote = "Aucun chasseur affecté.\n";
        }
        return strChasseurPilote;
    }
    
    /**
     * Ajout d'un pilote dans la hashmap
     * @author Kévin
     *   
     */
    public void inscriptionFormationPilote(Pilote pilote){
        pilotes.put(pilote.idPilote,pilote);//todo : mettre setter quand pilotes sera private
        }

    
    /**
     * La méthode ajoutNouveau permet d'ajouter un nouveau chasseur
     * @author Toutane
     *   
     */
    public void ajoutNouveau(Chasseur chasseur)
    {
        chasseurs.put(chasseur.idChasseur,chasseur);
    }
    

    /**
     * La méthode ajoutNouveau permet de changer l'état du chasseur 
     * (opérationnel,maintenace ou détruit)
     *  @author Toutane
     *  tests état valide/non valide en utilisant 
     *  un etat référé dans le tab et un non référé
     *  tests type valide/non valide
     *  un type référé dans le tab et un non référé
     */
    public void changerEtatChasseur(int idChasseur, String nouvelEtat)
    {
        chasseurs.get(idChasseur).etat = nouvelEtat;
    }
    /**
     * Vérifie si la hashmap chasseurs est vide ou pas
     *  @author Thierry
     */
    
    public boolean baseVideChasseurs()
    {
        if ( chasseurs.size() == 0 ) {
            return true;
        }
            else return false;
        }
        
    /**
     * Vérifie si la hashmap Pilotes est vide ou pas
     *  @author Thierry
     */
    public boolean baseVidePilotes()
    {
        if ( pilotes.size() == 0 ) {
            return true;
        }
            else return false;
        }
     
        /**
         * Affecte un pilote à un chasseur
         */

     public void affecterChasseur(int idPilote, int idChasseur){
        //récupération du chasseur et du pilote dans les bases de données chasseur et pilote
        Chasseur chasseurAaffecter = chasseurs.get(idChasseur);
        Pilote piloteAaffecter = pilotes.get(idPilote);
        //mise à jour des données du pilote
        piloteAaffecter.chasseur = chasseurAaffecter;
        piloteAaffecter.estAffecte = true;
        //mise à jour du pilote affecté dans la base de données pilotes
        pilotes.put(idPilote, piloteAaffecter);
        }
        
        /**
         * Renvoie un tableau d'Id Pilotes 
         */
        public String[] listerIdPilotes() {
            ArrayList<Integer> listeIdPilotes = new ArrayList<Integer>();
            for (Pilote pilote : pilotes.values())
            {
            listeIdPilotes.add(pilote.idPilote);
            }
            String[] tabIdPilotes = new String[listeIdPilotes.size()];
            int i = 0;
            for (Integer idPilote : listeIdPilotes)
            {
            String strId = String.valueOf(idPilote);
            tabIdPilotes[i] = strId ;
            i++;
            }
            return tabIdPilotes;
            }

            /**
             * Renvoie un tableau d'Id Chasseurs
             */
        public String[] listerIdChasseurs() {
            ArrayList<Integer> listeIdChasseurs = new ArrayList<Integer>();
            for (Chasseur chasseur : chasseurs.values())
            {
            listeIdChasseurs.add(chasseur.idChasseur);
            }
            String[] tabIdChasseurs = new String[listeIdChasseurs.size()];
            int i = 0;
            for (Integer idPilote : listeIdChasseurs)
            {
            String strId = String.valueOf(idPilote);
            tabIdChasseurs[i] = strId ;
            i++;
            }
            return tabIdChasseurs;
        } 
}
