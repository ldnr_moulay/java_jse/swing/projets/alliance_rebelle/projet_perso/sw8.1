
/**
 * Décrivez votre classe App ici.
 *
 * @author Jérémy
 * @version (un numéro de version ou une date)
 */
public class App
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    static BaseDeDonnee bdd = new BaseDeDonnee();
    
    //tableau des types de chasseur possible 
    
    //tableau des états possibles d'un chasseur

    
    static Menu menu;
    
    /**
     * Lancement de l'application
     */
    public static void start()
    {
            
        
            //lancement du son et du Thread son
            Sound sound = new Sound("imperial_march.wav");
            Thread threadSound = sound.play();
            
            //Affichage du menu principal
            menu = new Menu(bdd, threadSound);
            menu.afficher();
            /*
            //TEST AVEC LA CREATION DE 3 PILOTES ET 3 CHASSEURS
            for (int i=1; i<4 ; i++)
            {
            Pilote pilote = new Pilote ("nom"+i,"prenom"+i,"humains"+i,200);
            bdd.inscriptionFormationPilote(pilote);
            Chasseur chasseur = new Chasseur ("type"+i,"en maintenance");
            bdd.ajoutNouveau(chasseur);
            }
            
            //TEST D'AFFICHAGE DE TOUS LES PILOTES
            bdd.ListerPilotes();
            
            //TEST D'AFFICHAGE DE TOUS LES CHASSEURS
            bdd.ListerChasseurs();
            
            //TEST D'AFFICHAGE DU CHASSEUR 3
            bdd.afficherChasseur(3);
            
            //TEST D'AFFICHAGE DU PILOTE 2
            bdd.afficherPilote(2);
            
            //AJOUT D'UN NOUVEAU CHASSEUR OPERATIONNAL
            Chasseur chasseur = new Chasseur (Chasseur.types[0],Chasseur.etatsPossibles[2]);
            bdd.ajoutNouveau(chasseur);
            
            //AFFICHAGE DES CHASSEURS OPERATIONNELS
            bdd.ListerChasseursOperationnels(Chasseur.etatsPossibles);
           
            //CHANGER L'ETAT DU CHASSEUR 1 de "en maintenance" à "opérationnel"
            bdd.changerEtatChasseur(1, Chasseur.etatsPossibles[1]);
            
            //AFFFICHER DE NOUVEAU TOUS LES CHASSEURS OPERATIONNELS
            bdd.ListerChasseursOperationnels(Chasseur.etatsPossibles);
            
            //AFFECTATION DU PILOTE 2 AU CHASSEUR 4
            bdd.affecterChasseur(2,4);
            
            //AFFICHAGE DU PILOTE 2 MIS A JOUR
            bdd.afficherPilote(2);
            
            //TEST D'AFFICHAGE DES 2 LISTES D'ID PILOTES ET CHASSEURS
            System.out.println("Affichage des id pilotes et chasseurs");
            bdd.listerIdChasseurs();
            bdd.listerIdPilotes();
            */
            

    }
}
