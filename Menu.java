import javax.swing.*;
import java.awt.Font;
import java.io.File;
import java.awt.BorderLayout;
import java.awt.*;
import javafx.scene.layout.Border;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class Menu extends Background
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    JFrame frame;
    JLabel titre;
    Background background;
    BaseDeDonnee db;
    Thread threadSound;
    
    static Sound sound;
    
    private Chasseur chasseur;
    private Pilote pilote;
    private SousMenu sousMenu;
    private boolean sousMenuExist;

    /**
     * Constructeur d'objets de classe Menu
     */
    public Menu(BaseDeDonnee baseDeDonnee, Thread threadSound)
    {
        frame = new JFrame();
        background = new Background();
        this.db = baseDeDonnee;
        this.threadSound = threadSound;
        sousMenu= new SousMenu(baseDeDonnee, threadSound);
        sousMenuExist = false; 
    }


    public void afficher()
    {
        //Création du titre****************************************
        titre = new JLabel("Star Wars Project");
        String filename="Starjhol.ttf";
        Font font = null;
        try
        {
            try
            {
                font = Font.createFont(Font.TRUETYPE_FONT, new File(filename));
            }
            catch (java.io.IOException ioe)
            {
                ioe.printStackTrace();
            }
            font = font.deriveFont(75f);
        }
        catch (java.awt.FontFormatException ffe)
        {
            ffe.printStackTrace();
        }
        titre.setHorizontalAlignment(SwingConstants.CENTER);
        titre.setFont(font);
        titre.setForeground(new java.awt.Color(255, 249, 38));
        background.add(titre, BorderLayout.NORTH);
        
    //grille de boutons 3x3 avec marge pour les boutons
    GridLayout gridButtons = new GridLayout(3, 3, 20, 40);
    JPanel panelButtons = new JPanel(gridButtons);
    //Rendre transparent le fond de la grille
    panelButtons.setOpaque( false );
    //Mettre une marge au Panel EmptyBorder(int top, int left, int bottom, int right)
    panelButtons.setBorder(BorderFactory.createEmptyBorder(20,20,20,20)); 


    JButton ajoutChasseur = new JButton("<html><center><span style='font-size:1.2em'> Ajouter un nouveau chasseur </span></center></html>");
    ajoutChasseur.addActionListener(e-> {
        if (sousMenuExist==false) {
        sousMenu.ajoutChasseurMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(ajoutChasseur);
    
    JButton afficherChasseur = new JButton("<html><center><span style='font-size:1.2em'> Afficher un chasseur </span></center></html>");
    afficherChasseur.addActionListener(e -> {
        if (sousMenuExist==false) {
        sousMenu.afficherChasseurMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(afficherChasseur);
    
    JButton modifierEtatChasseur = new JButton("<html><center><span style='font-size:1.2em'> Changer l'état d'un chasseur </span></center></html>");
    modifierEtatChasseur.addActionListener(e -> {
        if (sousMenuExist==false) {
        sousMenu.ModifierEtatChasseurMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(modifierEtatChasseur);
    
    
    JButton affecterChasseurAPilote = new JButton("<html><center><span style='font-size:1.2em'> Affecter un chasseur à un pilote</span></center></html>");
    affecterChasseurAPilote.addActionListener(e -> {
        if (sousMenuExist==false) {
        sousMenu.affecterChasseurMenu(); 
        sousMenuExist=true;
        frame.dispose();
        }
    });
    panelButtons.add(affecterChasseurAPilote);
    
    JButton afficherTousChasseurs = new JButton("<html><center><span style='font-size:1.2em'> Afficher tous les chasseurs </span></center></html>");
    afficherTousChasseurs.addActionListener(e-> {
        String texte="";
        if (sousMenuExist==false) {
        sousMenu.listeChasseursMenu(texte); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(afficherTousChasseurs);
    
    JButton listeChasseurOpe = new JButton("<html><center><span style='font-size:1.2em'> Afficher les chasseurs opérationnels</span></center></html>");
    listeChasseurOpe.addActionListener(e -> {
        String texte="";
        if (sousMenuExist==false) {
        sousMenu.listeChasseursOperationnelsMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(listeChasseurOpe);
    
    //Bouttons toolbar3
    
    JButton inscrireRebellePilote = new JButton("<html><center><span style='font-size:1.2em'> Inscrire un rebelle<br/>à la formation de pilote</span></center></html>");
    inscrireRebellePilote.addActionListener(e -> {
        if (sousMenuExist==false) {
        sousMenu.inscriptionRebelleMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(inscrireRebellePilote);
    
    JButton afficherInfosPilote = new JButton("<html><center><span style='font-size:1.2em'> Afficher un pilote </span></center></html>");
    afficherInfosPilote.addActionListener(e -> {
        if (sousMenuExist==false) {
        sousMenu.afficherPiloteMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(afficherInfosPilote);
    
    JButton afficherInfosPilotes = new JButton("<html><center><span style='font-size:1.2em'> Afficher tous les pilotes </span></center></html>");
    afficherInfosPilotes.addActionListener(e -> {
        String texte="";
        if (sousMenuExist==false) {
        sousMenu.listePilotesMenu(); 
        sousMenuExist=true;
        frame.dispose();
    }
    });
    panelButtons.add(afficherInfosPilotes);
    
    //création d'un bouton mute
    ImageIcon icon1 = new ImageIcon("volume-off.png");
    JLabel labelImage1 = new JLabel(icon1);
    labelImage1.setBorder(BorderFactory.createEmptyBorder(5, 0, 10, 0)); //createEmptyBorder(int top, int left, int bottom, int right)
    labelImage1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); //pour avoir le cursor "main" au survol de l'image
    
    //création d'un bouton volume on
    ImageIcon icon2 = new ImageIcon("volume-on.png");
    JLabel labelImage2 = new JLabel(icon2);
    labelImage2.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0)); //createEmptyBorder(int top, int left, int bottom, int right)
    labelImage2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); //pour avoir le cursor "main" au survol de l'image
    
    
    //Ajout de la grille au fond d'écran
    background.add(panelButtons);
    //Ajout de l'image au fond d'écran
    background.add(labelImage1,BorderLayout.SOUTH);
    //Ajout du fond d'écran à la fenetre
    frame.add(background);
    //Affichage de la fenetre SetBounds (x,y,width,height)
    frame.setBounds(210,100,900,500);
    frame.setVisible(true);
    
    //Evènements d'actions sur les boutons son au clic
    labelImage1.addMouseListener(new MouseAdapter() 
    {
    @Override
    public void mouseClicked(MouseEvent e) 
    {
        threadSound.stop();
        background.remove(labelImage1);
        background.add(labelImage2,BorderLayout.SOUTH);
        frame.add(background);
    }
    });
    
    labelImage2.addMouseListener(new MouseAdapter() 
    {
    public void mouseClicked(MouseEvent e) 
    {
        Sound sound = new Sound("imperial_march.wav");
        threadSound = sound.play();
        background.remove(labelImage2);
        background.add(labelImage1,BorderLayout.SOUTH);
        frame.add(background);
    }
    });
    
    }
       
}




