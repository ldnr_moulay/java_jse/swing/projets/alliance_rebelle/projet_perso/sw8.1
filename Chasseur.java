
/**
 * la classe Chasseur permet de créer un nouveu chasseur 
 * de type X-Wing ou Y-Wing avec un identifiant qui lui est propre.
 *
 * @author Toutane
 * 
 */
public class Chasseur
{
    // variables d'instance - 
    static int nbIdChasseur;
    static String[] etatsPossibles= { "Opérationnel", "En maintenance", "Détruit"};
    static String[] types = { "X-Wing", "Y-Wing"};
    int idChasseur;
    String type;
    String etat;
  
    /**
     * Constructeur Chasseur
     * @author Toutane
     */
    public Chasseur(String type,String etat)
    {
        nbIdChasseur++;
        this.idChasseur=nbIdChasseur;
        this.type=type;
        this.etat=etat;      
    }


}
