import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
* Décrivez votre classe Pilotes ici.
*
* @author Kevin
* @version (un numéro de version ou une date)
*/
public class Pilote extends Rebelle
{
    //propriétés
    BaseDeDonnee bdd;
    public Rebelle rebelle;
    public Chasseur chasseur;
    public static int nbIdPilote;
    public static String [] racesValide = {"Chalactéens","Chiss","Humains","Ithoriens","Mirialans","Kel_Dor","Kiffars","Miralukas","Mirialans","Nagais","Neimoidiens","Niktos","Noghris","Ongrees","Pau_ans","Quermiens","Rakata","Rodiens","Thisspasiens","Togrutas","Wookies","Wronians","Zabraks"};
    public int idPilote;
    public boolean estAffecte = false;
    public boolean peutPiloter = false;
    public boolean ageOk = false;
    //Constructors
    /**
    * Constructeur par defaut de Pilote
    * @author Kevin
    */
    
        public Pilote(){
    }
    
    /**
    * Constructeur de la classe Pilotes qui permet de créer des objets de Pilotes
    */
    public Pilote(String nomRebelle,String prenomRebelle, String raceRebelle, int ageRebelle){
    super(nomRebelle, prenomRebelle, raceRebelle, ageRebelle);
    this.chasseur=chasseur;
    nbIdPilote++;
    this.idPilote=nbIdPilote;
    this.estAffecte=false; 
    }
    
    Rebelle appelRebelle = new Rebelle();
    //BaseDeDonnee baseDeDonnee = new BaseDeDonnee();
    
    //Méthodes
    /**
    * Methode permettant de vérifier si un rebelle peut piloter ou non.
    * Le pilote doit etre age de 10 à 800 ans et doit avoir une race le permettant
    */
    
    public void peutPiloter(){
        ageOk();
        if (ageOk) {
            for (int i= 0; i<=racesValide.length;i++) {
                if(racesValide[i].equals(raceRebelle)){
                peutPiloter=true;
                }
            }
        }
    }
    
    public void ageOk(){
    if(appelRebelle.ageRebelle>=10 && appelRebelle.ageRebelle<=800){
    ageOk = true;
    }
    }

    //penser à mettre à jour la bdd pour le estAffecte
} 