
/**
 * Créé un rebelle, futur pilote ou jamais pilote
 *
 * @author Jérémy
 * @version (un numéro de version ou une date)
 */
public class Rebelle
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    String nomRebelle;
    String prenomRebelle;
    String raceRebelle;
    int ageRebelle;

    /**
     * Constructeur par défaut (gardé si besoin)
     * @author Jérémy
     */
    public Rebelle()
    {

    }
    
    /**
     * Constructeur utilisé par la classe pilote
     * Testé avec (Nom,Prénom,Humain,200)
     * @author Jérémy
     */
    public Rebelle(String nomRebelle,String prenomRebelle, String raceRebelle, int ageRebelle) {
    this.nomRebelle=nomRebelle;
    this.prenomRebelle=prenomRebelle;
    this.raceRebelle=raceRebelle;
    this.ageRebelle=ageRebelle;
    }
}
