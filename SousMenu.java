import java.awt.Dimension;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.*;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javafx.event.Event;
import java.awt.event.ActionEvent;
import javafx.scene.control.ComboBox;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JScrollPane;

/**
 * Crée les sous-menus
 *
 * @author Jérémy
 * @version (un numéro de version ou une date)
 */
public class SousMenu
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    String titreFrame = "Alliance rebelle";
    String titrePanneau = "Bienvenue dans l'alliance rebelle camarade";
    String texteParDefaut = "________Texte par defaut_________";
    String affichageParDefaut = "En attente d'une saisie";
    String boutonSaisie ="Valider la saisie";
    JFrame frame;
    JLabel label;
    Dimension d;
    JPanel centre;
    JPanel contentPane;
    Menu menu;
    BaseDeDonnee bdd;
    Thread threadSound;
    // String.valueOf() > id chasseur id pilote;  
      private String getTitreFrame() {
         return titreFrame;
    } 
      private String getTitrePanneau() {
         return titrePanneau;
    } 
    
    /**
     * Récupération de la base de données et du thread Sound
     */
       public SousMenu(BaseDeDonnee bdd, Thread threadSound) {
           this.bdd = bdd;
        }
    
    /**
     * Squelettes d'organisation pour l'affichage des Panels et des Labels
     */
    public void debutDeFrame()
    {
        frame = new JFrame(getTitreFrame());
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        contentPane = (JPanel)frame.getContentPane();
        contentPane.setLayout(new BorderLayout()); 
        JPanel haut = new JPanel();
        haut.setLayout(new FlowLayout());
        JButton retourAuMenu = new JButton("Revenir au menu");
        retourAuMenu.addActionListener(e -> {
            new Menu(bdd, threadSound).afficher();
            frame.dispose();
        });
        JLabel titre = new JLabel (getTitrePanneau()); 
        haut.add(titre);
        haut.add(retourAuMenu);
        contentPane.add(haut,BorderLayout.NORTH);
        centre = new JPanel ();
        contentPane.add(centre, BorderLayout.CENTER);   
    }
    
    /**
     * Réajuste le cadre et les éléments
     */
    public void finaliserLaFrame () {
         frame.setResizable(true);
        d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(500,500);
        frame.setLocation(d.width/2 - frame.getWidth()/2, d.height/2 - frame.getHeight()/2);
        frame.pack();
        frame.setVisible(true);
    }
    
    /**
     * Sous-menu qui liste l'ensemble des chasseurs
     */
    public void listeChasseursMenu(String texte)
    {
        debutDeFrame();
        JTextArea contenu = new JTextArea(affichageParDefaut);
        contenu.setEditable(false);
        centre.add(contenu);
        contenu.setText(bdd.ListerChasseurs());
        finaliserLaFrame();
    }
    
    /**
     * Sous-menu qui liste tous les chasseurs opérationnels
     */
    public void listeChasseursOperationnelsMenu()
    {
        debutDeFrame();
        JTextArea contenu = new JTextArea(affichageParDefaut);
        contenu.setEditable(false);
        centre.add(contenu);

        contenu.setText(bdd.ListerChasseursOperationnels(Chasseur.etatsPossibles));
        frame.pack();
        finaliserLaFrame();
    }
    
    /**
     * Sous-menu qui liste tous les pilotes formés
     */
    public void listePilotesMenu()
    {
        debutDeFrame();
        JTextArea contenu = new JTextArea(affichageParDefaut);
        centre.add(contenu);
        contenu.setText(bdd.ListerPilotes());
        frame.pack();
        finaliserLaFrame();
    }
    
    /**
     * Sous-menu qui affiche un pilote à partir de son identifiant
     */
    public void afficherPiloteMenu()
    {
        debutDeFrame();
        
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(3,1));
        
        JPanel flowCentre = new JPanel();
        flowCentre.setLayout(new FlowLayout());
        
        JLabel gridLabel = new JLabel("Pilote : ");
        flowCentre.add(gridLabel);
        
        grid.add(flowCentre);
        
        JComboBox c4 = new JComboBox(bdd.listerIdPilotes());
        flowCentre.add(c4);
        
        JTextArea contenuAffiche = new JTextArea("");
        contenuAffiche.setEditable(false);
        
        grid.add(contenuAffiche);
        
        JButton validerSaisie = new JButton(boutonSaisie);
        grid.add(validerSaisie);
        
        JTextArea texteSud = new JTextArea(affichageParDefaut);
        texteSud.setEditable(false);
        JPanel flowSud = new JPanel();
        flowSud.add(texteSud);
        contentPane.add(flowSud, BorderLayout.SOUTH);
        
        validerSaisie.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent evt) {
        String un = c4.getSelectedItem().toString();
        Integer idPilote = Integer.valueOf(un);
        contenuAffiche.setText(bdd.afficherPilote(idPilote));
        frame.pack();//bdd.afficherPilote(idPilote)
        texteSud.setText("Saisie OK");
    }
   });
        
        centre.add(grid);

        finaliserLaFrame();
    }
    
    /**
     * Sous-menu qui affiche un chasseur à partir de son identifiant
     */
        public void afficherChasseurMenu() 
    {
        debutDeFrame();
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(3,1));
        
        JPanel flowCentre = new JPanel();
        flowCentre.setLayout(new FlowLayout());
        
        JLabel gridLabel = new JLabel("Chasseur : ");
        flowCentre.add(gridLabel);
        
        grid.add(flowCentre);
        
         JComboBox c3 = new JComboBox(bdd.listerIdChasseurs());
        flowCentre.add(c3);
        
        JTextArea contenuAffiche = new JTextArea("");
        contenuAffiche.setEditable(false);
        
        grid.add(contenuAffiche);
        
        JButton validerSaisie = new JButton(boutonSaisie);
        grid.add(validerSaisie);
        centre.add(grid);
        
        JTextArea texteSud = new JTextArea(affichageParDefaut);
        texteSud.setEditable(false);
        JPanel flowSud = new JPanel();
        flowSud.add(texteSud);
        contentPane.add(flowSud, BorderLayout.SOUTH);
        
        validerSaisie.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent evt) {
        String un = c3.getSelectedItem().toString();
        Integer idChasseur = Integer.valueOf(un);
        contenuAffiche.setText(bdd.afficherChasseur(idChasseur));
        frame.pack();
        texteSud.setText("Saisie OK");
       }
    
    });

        finaliserLaFrame();
    }
    
    /**
     * Sous-menu qui permet de modifier l'état d'un chasseur
     */
    public void ModifierEtatChasseurMenu() // manque affichage 
    {
        debutDeFrame();
        
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(3,1));
        
        JPanel flow1 = new JPanel();
        flow1.setLayout(new FlowLayout());
        
        JLabel gridLabel = new JLabel("Chasseur : ");
        flow1.add(gridLabel);
        
        JComboBox c4 = new JComboBox(bdd.listerIdChasseurs());
        flow1.add(c4);
        
        grid.add(flow1);
        
        JPanel flow2 = new JPanel();
        flow2.setLayout(new FlowLayout());
        
        JLabel gridLabel2 = new JLabel("Etat : ");
        flow2.add(gridLabel2);
    
        JComboBox c1 = new JComboBox(Chasseur.etatsPossibles);
        flow2.add(c1);
        
        grid.add(flow2);
               
        JButton validerSaisie = new JButton(boutonSaisie);
        grid.add(validerSaisie);
        
        JTextArea texteSud = new JTextArea(affichageParDefaut);
        texteSud.setEditable(false);
        JPanel flowSud = new JPanel();
        flowSud.add(texteSud);
        contentPane.add(flowSud, BorderLayout.SOUTH);
        
        validerSaisie.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent evt) {
        String un = c4.getSelectedItem().toString();
        String deux = c1.getSelectedItem().toString();
        bdd.changerEtatChasseur(Integer.valueOf(un),deux);// ,(String) );
        texteSud.setText("Saisie OK");
   }
    
    });
        centre.add(grid);

        finaliserLaFrame();
    }
    
    /**
     * Sous-menu qui permet d'ajouter un chasseur à la base
     */
    public void ajoutChasseurMenu() //ok
    {
        debutDeFrame();
        Chasseur chasseur;
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(3,1));
        
        JPanel flow1 = new JPanel();
        flow1.setLayout(new FlowLayout());
        
        JLabel gridLabel = new JLabel("Type : ");
        flow1.add(gridLabel);
        
        grid.add(flow1);
        
        JComboBox c1 = new JComboBox(Chasseur.types);
        flow1.add(c1);
    
        JPanel flow2 = new JPanel();
        flow2.setLayout(new FlowLayout());
        
        JLabel gridLabel2 = new JLabel("Etat : ");
        flow2.add(gridLabel2);
            
        JComboBox c2 = new JComboBox(Chasseur.etatsPossibles);
        flow2.add(c2);
        grid.add(flow2);
          
        JButton validerSaisie = new JButton(boutonSaisie);
        JTextArea texteSud = new JTextArea(affichageParDefaut);
        JPanel flowSud = new JPanel();
        flowSud.add(texteSud);
        contentPane.add(flowSud, BorderLayout.SOUTH);
        validerSaisie.addMouseListener (new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                texteSud.setText("Saisie OK");
                String type = c1.getSelectedItem().toString();
                String etat = c2.getSelectedItem().toString();
                Chasseur chasseur = new Chasseur(type,etat);
                bdd.ajoutNouveau(chasseur);
            }
        });

        grid.add(validerSaisie);
        centre.add(grid);
        finaliserLaFrame();
    } 
    
    /**
     * Sous-menu qui affecte un chasseur à un pilote à partir de leurs identifiants
     */
     public void affecterChasseurMenu() 
     {
        debutDeFrame();
        
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(3,1));
        
        JPanel flow1 = new JPanel();
        flow1.setLayout(new FlowLayout());
        
        JLabel gridLabel = new JLabel("ID Chasseur : ");
        flow1.add(gridLabel);
        
        grid.add(flow1);
        
        JComboBox c3 = new JComboBox(bdd.listerIdChasseurs());
        flow1.add(c3);
        
        JPanel flow2 = new JPanel();
        flow2.setLayout(new FlowLayout());
        
        JLabel gridLabel2 = new JLabel("ID Pilote: ");
        flow2.add(gridLabel2);
        
        grid.add(flow2);;
        JComboBox c4 = new JComboBox(bdd.listerIdPilotes());
        flow2.add(c4);
        
        JButton validerSaisie = new JButton(boutonSaisie);
        grid.add(validerSaisie);
             
        centre.add(grid);
        
        JTextArea texteSud = new JTextArea(affichageParDefaut);
        texteSud.setEditable(false);
        JPanel flowSud = new JPanel();
        flowSud.add(texteSud);
        contentPane.add(flowSud, BorderLayout.SOUTH);
        
         validerSaisie.addMouseListener (new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                texteSud.setText("Saisie OK");
                String chasseur = c3.getSelectedItem().toString();
                String pilote = c4.getSelectedItem().toString();
                try {
                int idChasseur = Integer.valueOf(chasseur); 
                int idPilote = Integer.valueOf(pilote);
                bdd.affecterChasseur(idPilote,idChasseur);
                texteSud.setText("Saisie OK");
            } catch (Exception e) {
                texteSud.setText("Saisie invalide");
            }
            }
        });

        finaliserLaFrame();
        }
        
        /**
         * Sous-menu qui ajoute un pilote 
         */
    public void inscriptionRebelleMenu() //erreur affichage+manque listener+ manque option autree
    {
        debutDeFrame();
        
        JPanel grid = new JPanel();
        grid.setLayout(new GridLayout(5,1));
        
        JPanel flow1 = new JPanel();
        flow1.setLayout(new FlowLayout());
        
        JLabel gridLabel = new JLabel("Nom : ");
        flow1.add(gridLabel);
        
         
        JTextField gridField1 = new JTextField(texteParDefaut);
        flow1.add(gridField1);
        
        grid.add(flow1);
        
        JPanel flow2 = new JPanel();
        flow2.setLayout(new FlowLayout());
        
        JLabel gridLabel2 = new JLabel("Prénom : ");
        flow2.add(gridLabel2);
        
             
        JTextField gridField2 = new JTextField(texteParDefaut);
        flow2.add(gridField2);
        
        grid.add(flow2);
        
        JPanel flow3 = new JPanel();
        flow3.setLayout(new FlowLayout());
        
        JLabel gridLabel3 = new JLabel("Age : ");
        flow3.add(gridLabel3);
        
             
        JTextField gridField3 = new JTextField(texteParDefaut);
        flow3.add(gridField3);
        
        grid.add(flow3);
        
        JPanel flow4 = new JPanel();
        flow4.setLayout(new FlowLayout());
        
        JLabel gridLabel4 = new JLabel("Race : ");
        flow4.add(gridLabel4);
        
        JComboBox c3 = new JComboBox(Pilote.racesValide);
        c3.addItem("Autre");
        flow4.add(c3);
        
        grid.add(flow4);
       
        JButton validerSaisie = new JButton(boutonSaisie);
        grid.add(validerSaisie);
        
        JTextArea texteSud = new JTextArea(affichageParDefaut);
        texteSud.setEditable(false);
        JPanel flowSud = new JPanel();
        
        flowSud.add(texteSud);
        contentPane.add(flowSud, BorderLayout.SOUTH);
        
        validerSaisie.addMouseListener (new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                
            String race = c3.getSelectedItem().toString();
                
                try {
                int age = Integer.valueOf(gridField3.getText()); 
                if (race.equals("Autre")) {
                throw new Exception ();
                }
                if (gridField1.getText().equals(texteParDefaut)||gridField2.getText().equals(texteParDefaut)||race.equals(texteParDefaut)) {
                throw new Exception ();
                }
                if (age <10 || age > 800) {
                   throw new Exception (); 
                }
                bdd.inscriptionFormationPilote(new Pilote(gridField1.getText(),gridField2.getText(), race, age));
                texteSud.setText("Saisie OK");
            } catch (Exception e) {
                texteSud.setText("Saisie invalide");
            }
            }
        });
        
        centre.add(grid);

        finaliserLaFrame();
    }
     /*   public void frameAvecImput() //int nombre, String[] imputs
        {
            debutDeFrame();
            JPanel grid = new JPanel();
            grid.setLayout(new GridLayout(this.inputs.length,1));
            for (int i =1;i<=this.inputs.length;i++) {
            JPanel flowCentre = new JPanel();
            flowCentre.setLayout(new FlowLayout());
            JLabel gridLabel = new JLabel(this.inputs[i-1]);
            flowCentre.add(gridLabel);
            JTextField gridField = new JTextField("________texte par defaut_______");
            gridField.addActionListener(e -> test());
            flowCentre.add(gridField);
            grid.add(flowCentre);
        }
            centre.add(grid);
            finDeFrameAvecInput();
            finaliserLaFrame();
        }*/
    }    
